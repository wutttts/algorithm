package com.aidabai.algorithm.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

/**
 * @author wutangsheng
 * @create 2020-09-27 15:43
 * @info 统一异常处理
 */
@Getter
public class BadRequestException extends RuntimeException {

    private Integer status = HttpStatus.BAD_REQUEST.value();

    public BadRequestException(String message) {
        super(message);
    }

    public BadRequestException(String message, HttpStatus status) {
        super(message);
        this.status = status.value();
    }
}
