package com.aidabai.algorithm.recommend.domain;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * al_recommend
 * @author 
 */
@Data
public class AlRecommend implements Serializable {
    /**
     * ID
     */
    private Integer id;

    /**
     * 用户ID
     */
    private Integer userId;

    /**
     * 题目ID
     */
    private Integer questionId;

    /**
     * 创建日期
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    private static final long serialVersionUID = 1L;
}