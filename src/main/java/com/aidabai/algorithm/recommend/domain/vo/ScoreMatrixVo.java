package com.aidabai.algorithm.recommend.domain.vo;

/**
 * @author wutangsheng
 * @create 2020-09-28 5:09
 * @info
 */
public class ScoreMatrixVo {

    /**
     * 题目ID
     */
    private Integer questionId;

    /**
     * 分数
     */
    private Integer score;
}
