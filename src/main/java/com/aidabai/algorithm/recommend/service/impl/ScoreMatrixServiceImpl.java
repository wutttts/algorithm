package com.aidabai.algorithm.recommend.service.impl;

import com.aidabai.algorithm.exception.BadRequestException;
import com.aidabai.algorithm.recommend.dao.ScoreMatrixDao;
import com.aidabai.algorithm.recommend.domain.ScoreMatrix;
import com.aidabai.algorithm.recommend.service.IScoreMatrixService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author wutangsheng
 * @create 2020-09-28 4:53
 * @info
 */
@Slf4j
@Service
public class ScoreMatrixServiceImpl implements IScoreMatrixService {
    @Autowired
    private ScoreMatrixDao scoreMatrixDao;


    @Override
    public int insertScore(List<ScoreMatrix> list) {
        int i = scoreMatrixDao.batchInsertScore(list);
        if (i == 0)
            throw new BadRequestException("数据插入失败");
        return i;
    }
}
