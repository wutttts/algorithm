package com.aidabai.algorithm.recommend.service.impl;

import com.aidabai.algorithm.recommend.dao.AlRecommendDao;
import com.aidabai.algorithm.recommend.domain.AlRecommend;
import com.aidabai.algorithm.recommend.service.IAlRecommendService;
import com.aidabai.algorithm.system.dao.LeetNewQuestionDao;
import com.aidabai.algorithm.system.domain.LeetNewQuestion;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author wutangsheng
 * @create 2020-09-28 3:56
 * @info
 */
@Service
@Slf4j
public class AlRecommendServiceImpl implements IAlRecommendService {
    @Autowired
    private LeetNewQuestionDao leetNewQuestionDao;

    @Autowired
    private AlRecommendDao alRecommendDao;

    @Override
    public List<LeetNewQuestion> recommendList() {
        return null;
    }

    private AlRecommend initData(){
        List<Integer> questionIds = leetNewQuestionDao.selectByRandom();
        for (Integer questionId : questionIds) {

        }
        return null;
    }
}
