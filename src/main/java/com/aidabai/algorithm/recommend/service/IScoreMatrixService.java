package com.aidabai.algorithm.recommend.service;

import com.aidabai.algorithm.recommend.domain.ScoreMatrix;

import java.util.List;

/**
 * @author wutangsheng
 * @create 2020-09-28 4:53
 * @info
 */
public interface IScoreMatrixService {
    int insertScore(List<ScoreMatrix> list);
}
