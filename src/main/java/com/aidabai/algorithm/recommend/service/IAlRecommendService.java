package com.aidabai.algorithm.recommend.service;

import com.aidabai.algorithm.system.domain.LeetNewQuestion;

import java.util.List;

/**
 * @author wutangsheng
 * @create 2020-09-28 3:56
 * @info
 */
public interface IAlRecommendService {

    List<LeetNewQuestion> recommendList();
}
