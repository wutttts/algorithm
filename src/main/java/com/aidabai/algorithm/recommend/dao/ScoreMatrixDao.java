package com.aidabai.algorithm.recommend.dao;

import com.aidabai.algorithm.recommend.domain.ScoreMatrix;

import java.util.List;

public interface ScoreMatrixDao {
    int deleteByPrimaryKey(Integer id);

    int insert(ScoreMatrix record);

    int insertSelective(ScoreMatrix record);

    ScoreMatrix selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(ScoreMatrix record);

    int updateByPrimaryKey(ScoreMatrix record);

    int batchInsertScore(List<ScoreMatrix> list);
}