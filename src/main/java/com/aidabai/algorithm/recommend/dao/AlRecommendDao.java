package com.aidabai.algorithm.recommend.dao;

import com.aidabai.algorithm.recommend.domain.AlRecommend;

public interface AlRecommendDao {
    int deleteByPrimaryKey(Integer id);

    int insert(AlRecommend record);

    int insertSelective(AlRecommend record);

    AlRecommend selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(AlRecommend record);

    int updateByPrimaryKey(AlRecommend record);
}