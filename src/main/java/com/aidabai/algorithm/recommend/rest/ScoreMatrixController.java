package com.aidabai.algorithm.recommend.rest;

import com.aidabai.algorithm.recommend.domain.ScoreMatrix;
import com.aidabai.algorithm.recommend.service.IScoreMatrixService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author wutangsheng
 * @create 2020-09-28 4:51
 * @info
 */
@RestController
@RequestMapping("/api/score")
@Api(tags = "评分矩阵模块")
public class ScoreMatrixController {
    @Autowired
    private IScoreMatrixService scoreMatrixService;

    @PostMapping
    @ApiOperation("插入分数矩阵数据源")
    public ResponseEntity<Object> insertScore(String data){
        String str = "[{\"id\":10000,\"scorc\":\"这是一个分数\",\"fenshu\":2},{\"id\":10001,\"scorc\":\"这是一个分数\",\"fenshu\":1},{\"id\":10002,\"scorc\":\"这是一个分数\",\"fenshu\":3},{\"id\":10003,\"scorc\":\"这是一个分数\",\"fenshu\":1},{\"id\":10004,\"scorc\":\"这是一个分数\",\"fenshu\":2},{\"id\":10005,\"scorc\":\"这是一个分数\",\"fenshu\":1},{\"id\":10006,\"scorc\":\"这是一个分数\",\"fenshu\":2},{\"id\":10007,\"scorc\":\"这是一个分数\",\"fenshu\":1},{\"id\":10008,\"scorc\":\"这是一个分数\",\"fenshu\":2},{\"id\":10009,\"scorc\":\"这是一个分数\",\"fenshu\":1}]";
        str.replace("[","");
        str.replace("]","");
        List<ScoreMatrix> parse = (List<ScoreMatrix>) JSON.parse(str);
        int i = scoreMatrixService.insertScore(parse);
        return ResponseEntity.ok(i);
    }

}
