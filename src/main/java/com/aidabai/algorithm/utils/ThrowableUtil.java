package com.aidabai.algorithm.utils;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * @author wutangsheng
 * @create 2020-09-27 15:51
 * @info
 */
public class ThrowableUtil {
    /**
     * 获取堆栈信息
     */
    public static String getStackTrace(Throwable throwable){
        StringWriter sw = new StringWriter();
        try (PrintWriter pw = new PrintWriter(sw)) {
            throwable.printStackTrace(pw);
            return sw.toString();
        }
    }
}
