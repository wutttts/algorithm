package com.aidabai.algorithm.utils;

import com.aidabai.algorithm.exception.BadRequestException;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;

import java.util.Objects;

/**
 * @author wutangsheng
 * @create 2020-09-27 16:58
 * @info 错误参数日志输出
 */
@Slf4j
public class BindingErrorLogUtil {

    public static void logError(BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            log.error("提交的参数有误,{} {}",
                    Objects.requireNonNull(bindingResult.getFieldError().getField(),
                            bindingResult.getFieldError().getDefaultMessage())
            );
            throw new BadRequestException(bindingResult.getFieldError().getDefaultMessage());
        }
    }
}
