package com.aidabai.algorithm.utils;

import com.aidabai.algorithm.config.MaxComputeConfig;
import com.aliyun.odps.Odps;
import com.aliyun.odps.Table;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wutangsheng
 * @create 2020-09-28 3:52
 * @info MaxCompute的工具类
 */
public class MaxComputeUtil {

    /**
     * 查询MaxCompute所有表。
     * @return
     */
    public static List<String> getTableName(){
        Odps odps = MaxComputeConfig.odps();
        List<String> names = new ArrayList<>();
        for (Table t : odps.tables()) {
            names.add(t.getName());
        }
        return names;
    }

    /**
     * TODO 一些表操作
     */

}
