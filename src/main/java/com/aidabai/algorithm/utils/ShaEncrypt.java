package com.aidabai.algorithm.utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author wutangsheng
 * @create 2020-09-28 3:34
 * @info sha加密
 */
public class ShaEncrypt {
    public static String encryption(String target) throws NoSuchAlgorithmException {
        //sha加密密码
        MessageDigest md = MessageDigest.getInstance("SHA");
        md.update(target.getBytes());
        String result = new BigInteger(md.digest()).toString(32);
        return result;
    }
}
