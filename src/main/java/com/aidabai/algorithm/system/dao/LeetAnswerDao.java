package com.aidabai.algorithm.system.dao;

import com.aidabai.algorithm.system.domain.LeetAnswer;

public interface LeetAnswerDao {
    int deleteByPrimaryKey(Integer id);

    int insert(LeetAnswer record);

    int insertSelective(LeetAnswer record);

    LeetAnswer selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(LeetAnswer record);

    int updateByPrimaryKey(LeetAnswer record);
}