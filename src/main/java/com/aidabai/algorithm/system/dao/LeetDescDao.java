package com.aidabai.algorithm.system.dao;

import com.aidabai.algorithm.system.domain.LeetDesc;

public interface LeetDescDao {
    int deleteByPrimaryKey(Integer id);

    int insert(LeetDesc record);

    int insertSelective(LeetDesc record);

    LeetDesc selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(LeetDesc record);

    int updateByPrimaryKey(LeetDesc record);
}