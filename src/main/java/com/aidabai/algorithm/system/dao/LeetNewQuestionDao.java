package com.aidabai.algorithm.system.dao;

import com.aidabai.algorithm.system.domain.LeetNewQuestion;

import java.util.List;

public interface LeetNewQuestionDao {
    int deleteByPrimaryKey(Integer id);

    int insert(LeetNewQuestion record);

    int insertSelective(LeetNewQuestion record);

    LeetNewQuestion selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(LeetNewQuestion record);

    int updateByPrimaryKey(LeetNewQuestion record);

    List<Integer> selectByRandom();

    List<LeetNewQuestion> queryQuestion();

    List<LeetNewQuestion> getAll();
}