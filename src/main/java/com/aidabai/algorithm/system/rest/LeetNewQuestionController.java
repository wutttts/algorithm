package com.aidabai.algorithm.system.rest;

import com.aidabai.algorithm.system.domain.LeetNewQuestion;
import com.aidabai.algorithm.system.service.ILeetNewQuestionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author wutangsheng
 * @create 2020-09-28 4:25
 * @info
 */
@RestController
@Slf4j
@RequestMapping("/api/question")
@Api(tags = "推荐模块")
public class LeetNewQuestionController {
    @Autowired
    private ILeetNewQuestionService leetNewQuestionService;

    @GetMapping("/test")
    @ApiOperation("能力测试接口")
    public ResponseEntity<Object> query(){
        List<LeetNewQuestion> questions = leetNewQuestionService.abilityTestQuestion();
        return ResponseEntity.ok(questions);
    }

    @GetMapping
    @ApiOperation("根据id获取题目信息")
    public ResponseEntity<Object> queryById(Integer id){
        LeetNewQuestion questions = leetNewQuestionService.queryQuestion(id);
        return ResponseEntity.ok(questions);
    }

    @GetMapping("/all")
    @ApiOperation("根据id获取题目信息")
    public ResponseEntity<Object> getAll(){
        List<LeetNewQuestion> allQuestion = leetNewQuestionService.getAllQuestion();
        return ResponseEntity.ok(allQuestion);
    }
}
