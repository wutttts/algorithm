package com.aidabai.algorithm.system.rest;

import com.aidabai.algorithm.exception.BadRequestException;
import com.aidabai.algorithm.system.consts.AlgorithmConst;
import com.aidabai.algorithm.system.domain.User;
import com.aidabai.algorithm.system.domain.vo.UserVo;
import com.aidabai.algorithm.system.service.IUserService;
import com.aidabai.algorithm.utils.BindingErrorLogUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.NoSuchAlgorithmException;

/**
 * @author wutangsheng
 * @create 2020-09-27 15:58
 * @info
 */
@RestController
@RequestMapping("/api/user")
@Api(tags = "用户模块")
@Slf4j
public class UserController {
    @Autowired
    private IUserService userService;

    @PostMapping("/register")
    @ApiOperation("注册接口")
    public <bindingResult> ResponseEntity<Object> register(@Valid @RequestBody UserVo userVo, BindingResult bindingResult) throws NoSuchAlgorithmException {
        log.info("userVo {}",userVo);
        BindingErrorLogUtil.logError(bindingResult);
        userService.Register(userVo);
        return new ResponseEntity<>("注册成功", HttpStatus.OK);
    }

    @PostMapping("/login")
    @ApiOperation("登录接口")
    public ResponseEntity login(@Valid @RequestBody UserVo userVo, HttpServletRequest request) throws NoSuchAlgorithmException {
        log.info("userVo {}",userVo);
        User userInfo = userService.login(userVo);
        //设置session
        HttpSession session = request.getSession();
        session.setAttribute(AlgorithmConst.CURRENT_USER,userVo);
        return new ResponseEntity<>(userInfo, HttpStatus.OK);
    }

    @GetMapping
    @ApiOperation("根据id获取用户信息")
    public ResponseEntity<Object> query(Integer id) {
        return ResponseEntity.ok(userService.getUserByPrimaryKey(id));
    }

    @GetMapping("/logininfo")
    @ApiOperation("获取登录用户信息")
    public ResponseEntity<Object> query(HttpSession session){
        User user =(User) session.getAttribute(AlgorithmConst.CURRENT_USER);
        if (user == null)
            throw new BadRequestException("请先登录",HttpStatus.UNAUTHORIZED);
        return ResponseEntity.ok(user);
    }
}
