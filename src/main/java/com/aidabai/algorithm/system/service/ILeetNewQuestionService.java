package com.aidabai.algorithm.system.service;

import com.aidabai.algorithm.system.domain.LeetNewQuestion;
import io.swagger.models.auth.In;

import java.util.List;

/**
 * @author wutangsheng
 * @create 2020-09-28 3:29
 * @info
 */
public interface ILeetNewQuestionService {
    List<LeetNewQuestion> abilityTestQuestion();

    LeetNewQuestion queryQuestion(Integer id);

    List<LeetNewQuestion> getAllQuestion();
}
