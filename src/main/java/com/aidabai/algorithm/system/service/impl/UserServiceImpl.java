package com.aidabai.algorithm.system.service.impl;

import com.aidabai.algorithm.exception.BadRequestException;
import com.aidabai.algorithm.system.dao.UserDao;
import com.aidabai.algorithm.system.domain.User;
import com.aidabai.algorithm.system.domain.vo.UserVo;
import com.aidabai.algorithm.system.service.IUserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author wutangsheng
 * @create 2020-09-27 15:39
 * @info
 */
@Service
@Slf4j
public class UserServiceImpl implements IUserService {

    @Autowired
    private UserDao userDao;


    @Override
    public void Register(UserVo userVo) throws NoSuchAlgorithmException {
        int count = userDao.countByUsername(userVo.getUsername());
        if (count == 1)
            throw new BadRequestException("用户已存在");

        String password = userVo.getPassword();
        String newPassword = encryption(password);

        User user = new User(userVo.getUsername(), newPassword);
        int i = userDao.insertSelective(user);
        if (i == 0)
            throw new BadRequestException("注册失败，刷新页面再试一下");
    }

    @Override
    public User login(UserVo userVo) throws NoSuchAlgorithmException {
        User user = userDao.selectByUsername(userVo.getUsername());
        String encryption = encryption(userVo.getPassword());
        if (user == null)
            throw new BadRequestException("用户名或密码错误", HttpStatus.UNAUTHORIZED);
        if (!user.getPassword().equalsIgnoreCase(encryption))
            throw new BadRequestException("用户名或密码错误", HttpStatus.UNAUTHORIZED);
        user.setPassword("");
        return user;
    }

    @Override
    public User getUserByPrimaryKey(Integer id) {
        User user = userDao.selectByPrimaryKey(id);
        if (user == null)
            throw new BadRequestException("没有找到该用户");
        return user;
    }

    private String encryption(String target) throws NoSuchAlgorithmException {
        //sha加密密码
        MessageDigest md = MessageDigest.getInstance("SHA");
        md.update(target.getBytes());
        String result = new BigInteger(md.digest()).toString(32);
        return result;
    }
}
