package com.aidabai.algorithm.system.service.impl;

import com.aidabai.algorithm.exception.BadRequestException;
import com.aidabai.algorithm.system.dao.LeetNewQuestionDao;
import com.aidabai.algorithm.system.domain.LeetNewQuestion;
import com.aidabai.algorithm.system.service.ILeetNewQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author wutangsheng
 * @create 2020-09-28 3:29
 * @info
 */
@Service
public class LeetNewQuestionImpl implements ILeetNewQuestionService {
    @Autowired
    private LeetNewQuestionDao leetNewQuestionDao;

    @Override
    public List<LeetNewQuestion> abilityTestQuestion() {
        List<LeetNewQuestion> leetNewQuestions = leetNewQuestionDao.queryQuestion();
        if (leetNewQuestions == null)
            throw new BadRequestException("数据异常");
        if (leetNewQuestions.size() == 0)
            throw new BadRequestException("无数据");
        return leetNewQuestions;
    }

    @Override
    public LeetNewQuestion queryQuestion(Integer id) {
        LeetNewQuestion leetNewQuestion = leetNewQuestionDao.selectByPrimaryKey(id);
        if (leetNewQuestion == null)
            throw new BadRequestException("数据返回异常");
        return leetNewQuestion;
    }

    @Override
    public List<LeetNewQuestion> getAllQuestion() {
        List<LeetNewQuestion> all = leetNewQuestionDao.getAll();
        if (all == null)
            throw new BadRequestException("数据异常");
        if (all.size() == 0)
            throw new BadRequestException("无数据");
        return all;
    }
}
