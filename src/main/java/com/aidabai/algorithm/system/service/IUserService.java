package com.aidabai.algorithm.system.service;

import com.aidabai.algorithm.system.domain.User;
import com.aidabai.algorithm.system.domain.vo.UserVo;

import java.security.NoSuchAlgorithmException;

/**
 * @author wutangsheng
 * @create 2020-09-27 15:38
 * @info
 */
public interface IUserService {

    void Register(UserVo userVo) throws NoSuchAlgorithmException;

    User login(UserVo userVo) throws NoSuchAlgorithmException;

    User getUserByPrimaryKey(Integer id);
}
