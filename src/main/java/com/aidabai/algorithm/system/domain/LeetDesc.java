package com.aidabai.algorithm.system.domain;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * leet_desc
 * @author 
 */
@Data
public class LeetDesc implements Serializable {
    /**
     * 解法id
     */
    @ApiModelProperty(value = "用户ID",hidden = true)
    private Integer id;

    /**
     * 信息描述
     */
    @ApiModelProperty(value = "信息描述")
    private String dataDesc;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    private static final long serialVersionUID = 1L;
}