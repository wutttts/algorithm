package com.aidabai.algorithm.system.domain.vo;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * @author wutangsheng
 * @create 2020-09-27 16:10
 * @info
 */

@Getter
@Setter
public class UserVo {

    @NotBlank
    private String username;


    @NotBlank
    private String password;


    @Override
    public String toString() {
        return "UserVo{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
