package com.aidabai.algorithm.system.domain;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * leet_new_question
 * @author 
 */
@Data
public class LeetNewQuestion implements Serializable {

    @ApiModelProperty(value = "用户id",hidden = true)
    private Integer id;

    @ApiModelProperty(value = "题目标题")
    private String title;

    @ApiModelProperty(value = "题目内容")
    private String content;

    @ApiModelProperty(value = "分类")
    private String category;

    private static final long serialVersionUID = 1L;
}