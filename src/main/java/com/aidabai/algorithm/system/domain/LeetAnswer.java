package com.aidabai.algorithm.system.domain;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * leet_answer
 * @author 
 */
@Data
public class LeetAnswer implements Serializable {
    /**
     * 解法id
     */
    private Integer id;

    /**
     * 题目id
     */
    private Integer questionId;

    /**
     * 解法标题
     */
    private String title;

    /**
     * 解法详情
     */
    private String content;

    /**
     * 状态0-正常 1-异常
     */
    private Integer status;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    private static final long serialVersionUID = 1L;
}