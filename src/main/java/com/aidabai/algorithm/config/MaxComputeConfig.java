package com.aidabai.algorithm.config;

import com.aliyun.odps.Odps;
import com.aliyun.odps.account.Account;
import com.aliyun.odps.account.AliyunAccount;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @author wutangsheng
 * @create 2020-09-28 3:39
 * @info MaxCompute连接配置。
 */
@Configuration
@Data
public class MaxComputeConfig {
    @Value("${odps.access-id}")
    private static String accessId;

    @Value("${odps.access-key}")
    private static String accessKey;

    @Value("${odps.endpoint}")
    private static String endpoint;

    @Value("${odps.tunnel-endpoint}")
    private static String tunnelEndpoint;

    @Value("${odps.project-name}")
    private static String projectName;

    public static Odps odps(){
        Account account = new AliyunAccount(accessId, accessKey);
        Odps odps = new Odps(account);
        odps.setEndpoint(endpoint);
        odps.setDefaultProject(projectName);
        return odps;
    }
}
